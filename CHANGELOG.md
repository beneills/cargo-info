## 0.4.0

Report crate version history with -V/--versions flag

## 0.3.4

Decode timestamps and report in more human readable form

## 0.3.3

No new functionality - dependency updates

## 0.3.2

No new functionality - dependency updates

## 0.3.1

Make -jv print pretty JSON

## 0.3.0

Add support for raw JSON output (-j)
Replace serde with json for JSON decode
Support stable Rust channel

## 0.2.0

Add flags for getting a specific crate detail
-H - homepage
-d - documentation
-D - downloads
-r - repository

## 0.1.2

Add support for -v

## 0.0.1

Initial drop
